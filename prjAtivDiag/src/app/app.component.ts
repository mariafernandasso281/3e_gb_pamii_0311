import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: 'home' },
    { title: 'Entrar', url: 'login'},
    { title: 'Cadastrar-se', url: 'cadastro' },
    { title: 'Imóveis Disponiveis', url: 'lista-imoveis'}
    
  ];
  constructor() {}
}
