import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { DestaqueComponent } from './destaque/destaque.component';
import { SearchdivComponent } from './searchdiv/searchdiv.component';
import { ImoveisNovosComponent } from './imoveis-novos/imoveis-novos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, DestaqueComponent, SearchdivComponent, ImoveisNovosComponent]
})
export class HomePageModule {}
