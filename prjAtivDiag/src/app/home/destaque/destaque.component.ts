import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destaque',
  templateUrl: './destaque.component.html',
  styleUrls: ['./destaque.component.scss'],
})
export class DestaqueComponent implements OnInit {
  public card = [
    { title: ' O maior portal de imóveis do Brasil ', text: ' Encontrar o imóvel dos seus sonhos se torna uma experiência ainda melhor com mais de 5 milhões de opções que o CanMovel oferece. ' ,icon : 'business-outline'},
    { title: ' Encontre seu imóvel perfeito ', text: ' É possível buscar seu novo lar através dos filtros de palavra-chave, características e valor total de aluguel que o CanMovel mostra para você. ', icon: 'laptop-outline'},
    { title: ' Seu imóvel na palma da mão ', text: ' Quer buscar imóveis de qualquer lugar, falar com corretores, agendar visitas e ver facilidades ao redor do imóvel? Crie sua conta no CanMovel gratuitamente. ', icon: 'phone-portrait-outline' }
    
  ];

  constructor() { }

  ngOnInit() {}

}
